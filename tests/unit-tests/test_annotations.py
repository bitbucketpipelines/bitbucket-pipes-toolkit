import pytest
from requests.models import Response
from bitbucket_pipes_toolkit.annotations import CodeInsights


# Add the import for pytest-mock
pytest_plugins = ["pytest_mock"]


@pytest.fixture
def code_insights():
    return CodeInsights(repo="test-repo", username="test-user", auth_type="authless", token="test-token")


def test_create_report(mocker, code_insights):
    mock_response = mocker.Mock(spec=Response)
    mock_response.ok = True
    mock_response.json.return_value = {"key": "value"}
    mocker.patch.object(code_insights.http_client, 'make_session_request', return_value=mock_response)

    commit = "test-commit"
    report_data = {"uuid": "test-uuid", "external_id": "test-id", "title": "Test Report"}

    response = code_insights.create_report(commit, report_data)
    assert response == {"key": "value"}


def test_get_reports(mocker, code_insights):
    mock_response = mocker.Mock(spec=Response)
    mock_response.ok = True
    mock_response.json.return_value = {"reports": []}
    mocker.patch.object(code_insights.http_client, 'make_session_request', return_value=mock_response)

    commit = "test-commit"
    response = code_insights.get_reports(commit)
    assert response == {"reports": []}


def test_get_report(mocker, code_insights):
    mock_response = mocker.Mock(spec=Response)
    mock_response.ok = True
    mock_response.json.return_value = {"report": "data"}
    mocker.patch.object(code_insights.http_client, 'make_session_request', return_value=mock_response)

    commit = "test-commit"
    report_id = "test-report-id"
    response = code_insights.get_report(commit, report_id)
    assert response == {"report": "data"}


def test_delete_report(mocker, code_insights):
    mock_response = mocker.Mock(spec=Response)
    mock_response.ok = True
    mocker.patch.object(code_insights.http_client, 'make_session_request', return_value=mock_response)

    commit = "test-commit"
    report_id = "test-report-id"
    response = code_insights.delete_report(commit, report_id)
    assert response is True


def test_create_annotation(mocker, code_insights):
    mock_response = mocker.Mock(spec=Response)
    mock_response.ok = True
    mock_response.json.return_value = {"key": "value"}
    mocker.patch.object(code_insights.http_client, 'make_session_request', return_value=mock_response)

    commit = "test-commit"
    report_id = "test-report-id"
    annotation_data = {"uuid": "test-uuid", "external_id": "test-id", "summary": "Test Annotation"}

    response = code_insights.create_annotation(commit, report_id, annotation_data)
    assert response == {"key": "value"}


def test_create_bulk_annotations(mocker, code_insights):
    mock_response = mocker.Mock(spec=Response)
    mock_response.ok = True
    mock_response.json.return_value = [{"key": "value"}]
    mocker.patch.object(code_insights.http_client, 'make_session_request', return_value=mock_response)

    commit = "test-commit"
    report_id = "test-report-id"
    annotations_data = [{"uuid": "test-uuid", "external_id": "test-id", "summary": "Test Annotation"}]

    response = code_insights.create_bulk_annotations(commit, report_id, annotations_data)
    assert response == [{"key": "value"}]
