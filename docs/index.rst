.. Bitbucket Pipes Toolkit documentation master file, created by
   sphinx-quickstart on Fri May 31 11:50:32 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Bitbucket Pipes Toolkit's documentation!
===================================================

This library provides various utilities for writing and testing Bitbucket pipes.

Indices and tables
==================

* :ref:`installation`
* :ref:`interfaces`
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


.. _installation:

Installation
------------
.. code-block:: bash

    pip install bitbucket_pipes_toolkit


.. toctree::
   :maxdepth: 2
   :caption: Contents:

.. _interfaces:

Available classes and functions
-------------------------------

.. currentmodule:: bitbucket_pipes_toolkit

.. autosummary::

    Pipe
    CodeInsights



.. automodule:: bitbucket_pipes_toolkit
    :members:
    :inherited-members:

Examples
--------

Example subclissing and running the pipe:

.. code-block:: python3

    from bitbucket_pipes_toolkit import Pipe

    class MyPipe(Pipe):
        def run(self):
            super().run()
            print("I'm a simple little pipe")


    # below is a simple schema for pipe variables.
    schema = {'USERNAME': {'required': True, 'type': 'string'},
              'PASSWORD': {'required': True, 'type': 'string'}}
    
    my_pipe=MyPipe(pipe_metadata='pipe.yml', schema=schema)
    my_pipe=pipe.run()

Example writing a simple test case for a pipe

.. code-block:: python3

    from bitbucket_pipes_toolkit.test import PipeTestCase
    
    class PipelineTypeTestCase(TriggerBuildBaseTestCase):
    
    def test_something_is_successful(self):
        result = self.run_container(environment={
            'USERNAME': 'JohnDoe',
            'PASSWORD': os.getenv('PASSWORD'),
        })
    
        self.assertRegexpMatches(
            result, r"Something was successful!")





